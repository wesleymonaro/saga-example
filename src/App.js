import React, { Component, Fragment } from "react";
import { Provider } from "react-redux";

import { BrowserRouter } from "react-router-dom";

import "./styles/global";

import { Wrapper, Container } from "./styles/components";

import store from "./store";

const App = () => (
  <Provider store={store}>
    <BrowserRouter>
      <Wrapper>
        <Container>
          <h1>hello Saga</h1>
        </Container>
      </Wrapper>
    </BrowserRouter>
  </Provider>
);

export default App;
