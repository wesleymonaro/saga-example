import { combineReducers } from "redux";

import error from "./error";
import example from "./example";

export default combineReducers({
  error,
  example
});
