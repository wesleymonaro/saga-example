import Sound from "react-sound";

export const Types = {
  ACTION: "example/ACTION"
};

const INITIAL_STATE = {
  data: ""
};

export default function player(state = INITIAL_STATE, action) {
  switch (action.type) {
    case Types.ACTION:
      return {};

    default:
      return state;
  }
}

export const Creators = {
  getTeste: data => ({
    type: Types.ACTION,
    payload: { data }
  })
};
