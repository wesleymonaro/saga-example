import { all, takeLatest } from "redux-saga/effects";

import { Types as ErrorTypes } from "../ducks/error";
import { Types as ExampleTypes } from "../ducks/example";
import { getExample } from "./example";

export default function* rootSaga() {
  yield all([takeLatest(ExampleTypes.GET_REQUEST, getExample)]);
}
