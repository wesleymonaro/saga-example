import { call, put } from "redux-saga/effects";
import api from "../../services/api";

import { Creators as ExampleActions } from "../ducks/example";
import { Creators as ErrorActions } from "../ducks/error";

export function* getExample() {
  try {
    const response = yield call(api.get, "/teste");

    yield put(ExampleActions.getTeste(response.data));
  } catch (error) {
    yield put(ErrorActions.setError("Ocorreu um erro"));
  }
}
